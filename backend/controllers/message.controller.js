const { Conversation } = require("../models/conversation.model");
const { Message } = require("../models/message.model");
const { io, getReceiverSocketId } = require("../socket/socket");

module.exports.sendMessage = async function (req, res) {
  const { id: receiverId } = req.params;
  const { message } = req.body;
  const senderId = req.user._id;

  try {
    let conversation = await Conversation.findOne({
      participants: { $all: [senderId, receiverId] }
    });

    if (!conversation) {
      conversation = await Conversation.create({
        participants: [senderId, receiverId],
      });
    };

    const newMessage = new Message({
      senderId: senderId,
      receiverId: receiverId,
      message: message
    });

    if (newMessage) {
      conversation.messages.push(newMessage._id);
    }

    // this will run in parallel
    await Promise.all([conversation.save(), newMessage.save()]);

    const receiverSocketId = getReceiverSocketId(receiverId);
    if (receiverSocketId) {
      io.to(receiverSocketId).emit("newMessage", newMessage);
    }

    res.status(201).json({ success: true, message: "Message sent", newMessage });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal Server Error" });
  }
};

module.exports.getMessages = async function (req, res) {
  const senderId = req.user._id;
  const { id: userToChatId } = req.params;
  try {
    const conversation = await Conversation.findOne({
      participants: { $all: [senderId, userToChatId] }
    }).populate("messages");

    if (!conversation) return res.status(200).json([]);

    const messages = conversation.messages;

    return res.status(200).json(messages);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal Server Error" });
  }
};