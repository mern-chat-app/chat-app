const bcrypt = require("bcrypt");
const User = require("../models/user.model");
const generateTokenAndSetCookie = require("../utils/generateToken");

module.exports.signup = async (req, res) => {
  const { fullName, username, password, confirmPassword, gender } = req.body;
  try {
    if (password !== confirmPassword) {
      return res.status(400).json({ success: false, message: "Password don't match" });
    }

    // lets find the unique username
    const user = await User.findOne({ username });
    if (user) return res.status(400).json({ success: false, message: "Username already exists" });

    // lets hash the password...
    const genSalt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, genSalt);

    // lets first decide the profile pic according to the gender
    const boyPic = `https://avatar.iran.liara.run/public/boy?username=${username}`;
    const girlPic = `https://avatar.iran.liara.run/public/girl?username=${username}`;

    // now lets create a user.
    const newUser = new User({
      fullname: fullName,
      username,
      password: hashedPassword,
      gender: gender,
      profilePic: gender === "male" ? boyPic : girlPic
    });

    if (newUser) {
      // generate JWT token here
      generateTokenAndSetCookie(newUser._id, res);
      await newUser.save();

      res.status(201).json({
        success: true,
        user: {
          _id: newUser._id,
          fullname: newUser.fullname,
          username: newUser.username,
          profilePic: newUser.profilePic
        }
      });
    } else {
      return res.status(400).json({ success: false, message: "Invalid user data" })
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal Server Error" });
  }
};

module.exports.login = async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({ username: username });
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!user || !passwordMatch) return res.status(400).json({ success: false, message: "Invalid username or password" });

    generateTokenAndSetCookie(user._id, res);

    res.status(200).json({
      success: true,
      user: {
        _id: user._id,
        fullname: user.fullname,
        username: user.username,
        profilePic: user.profilePic
      }
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal Server Error" });
  }
};

module.exports.logout = async (req, res) => {
  try {
    res.cookie("jwt", "", { maxAge: 0 });
    return res.status(200).json({ success: true, message: "User logged out" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal Server Error" });
  }
};