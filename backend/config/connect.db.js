const mongoose = require("mongoose");
require("dotenv").config();
const url = process.env.MONGO_URL;

const connectDb = () =>
  mongoose.connect(url)
    .then(() => console.log("Connected to Database"))
    .catch(error => console.log(error));

module.exports = connectDb;