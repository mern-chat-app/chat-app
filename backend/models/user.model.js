const mongoose = require("mongoose");

const newUserSchema = new mongoose.Schema({
  fullname: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true,
    enum: ["male", "female"],
  },
  profilePic: {
    type: String,
    default: ""
  }
}, { timestamps: true });

const User = mongoose.model("User", newUserSchema);
module.exports = User;