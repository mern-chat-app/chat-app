const { getUsers } = require("../controllers/user.controller");
const { protectRoute } = require("../middleware/protectRoute");

const userRoute = require("express").Router();

userRoute.route("/").get(protectRoute, getUsers);

module.exports = userRoute;