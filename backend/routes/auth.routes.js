const express = require("express");
const { signup, logout, login } = require("../controllers/auth.controller");
const authRouter = express.Router();

authRouter.post("/login", login);
authRouter.post("/signup", signup);
authRouter.post("/logout", logout);

module.exports = authRouter;