const { sendMessage, getMessages } = require("../controllers/message.controller");
const { protectRoute } = require("../middleware/protectRoute");

const messageRoute = require("express").Router();

// this id is the receiver id...
messageRoute.route("/send/:id").post(protectRoute, sendMessage);
messageRoute.route("/get/:id").get(protectRoute, getMessages);

module.exports = messageRoute;