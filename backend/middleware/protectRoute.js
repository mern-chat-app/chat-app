const jwt = require("jsonwebtoken");
const User = require("../models/user.model");
require("dotenv").config();

module.exports.protectRoute = async function (req, res, next) {
  try {
    const token = req.cookies.jwt;

    if (!token) return res.status(401).json({ success: false, message: "Unauthorized - No Token Provided" });

    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    if (!decoded) return res.status(401).json({ success: false, message: "Unauthorized - Invalid JWT Token" });

    const user = await User.findById(decoded.userId).select("-password");
    if (!user) return res.status(404).json({ success: false, message: "User not found" });

    req.user = user;
    next();
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
};
