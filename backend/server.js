const express = require("express");
const dotenv = require("dotenv");
dotenv.config();

const port = process.env.PORT;
const cors = require("cors");
const cookieParser = require("cookie-parser");
const path = require("path");

const authRouter = require("./routes/auth.routes");
const connectDb = require("./config/connect.db");
const messageRoute = require("./routes/message.routes");
const userRoute = require("./routes/users.routes");
const { app, server } = require("./socket/socket");

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Testing api...
// app.get("/", async function (req, res) {
//   res.status(200).json({ success: true, message: "Server is running!" });
// });

const ____variableOfChoice = path.resolve();

app.use("/api/auth/", authRouter);
app.use("/api/users/", userRoute);
app.use("/api/message/", messageRoute);

app.use(express.static(path.join(____variableOfChoice, "/frontend/dist")));

app.get("*", async function (req, res) {
  res.sendFile(path.join(____variableOfChoice, "frontend", "dist", "index.html"));
})

server.listen(port, () => {
  connectDb();
  console.log("Server Started on Port " + port);
});
