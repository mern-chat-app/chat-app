import { useState } from "react";
import toast from "react-hot-toast";
import axios from "axios";
import { useAuthContext } from "../context/AuthContext";

const handleInputErrors = (username, password) => {
  if (!username || !password) {
    toast.error("Please fill all required fields");
    return false;
  };
  return true;
}

const useLogin = () => {
  const [loading, setLoading] = useState(false);
  const { setUserDetails } = useAuthContext();

  const login = async (username, password) => {
    const success = handleInputErrors(username, password);
    if (!success) return;
    setLoading(true);

    try {
      const { data } = await axios.post("/api/auth/login", { username, password });
      if (!data.success) return toast.error(data.message);

      setUserDetails(data.user);
    } catch (error) {
      console.log(error.response.data.message);
    } finally {
      setLoading(false);
    }

  };

  return { loading, login };
};

export default useLogin;