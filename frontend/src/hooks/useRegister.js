import React from "react";
import toast from "react-hot-toast";
import axios from "axios";
import { useAuthContext } from "../context/AuthContext";

function handleInputErrors({ fullName, username, password, confirmPassword, gender }) {
  if (!fullName || !username || !password || !confirmPassword || !gender) {
    toast.error("Please fill in all fields");
    return false;
  }

  if (password !== confirmPassword) {
    toast.error("Passwords do not match");
    return false;
  }

  if (password.length < 6) {
    toast.error("Password must be at least 6 characters");
    return false;
  }

  return true;
}

const useRegister = () => {
  const [loading, setLoading] = React.useState(false);
  const { setUserDetails } = useAuthContext();

  const signUp = async ({ fullName, username, password, confirmPassword, gender }) => {
    const success = handleInputErrors({ fullName, username, password, confirmPassword, gender });
    if (!success) return;
    setLoading(true);

    try {
      const { data } = await axios.post("/api/auth/signup", { fullName, username, password, confirmPassword, gender });

      if (!data.success) return toast.error(data.message);

      localStorage.setItem("chat-user", JSON.stringify(data.user));
      setUserDetails(data.user);
    } catch (error) {
      console.log(error);
      toast.error(error.response.data.message);
    } finally {
      setLoading(false);
    }
  }

  return { loading, signUp };
};

export default useRegister;