import axios from "axios";
import React from "react";
import toast from "react-hot-toast";

const useGetConversations = () => {
  const [loading, setLoading] = React.useState();
  const [conversations, setConversations] = React.useState([]);

  React.useEffect(() => {
    const getConversations = async () => {
      setLoading(true);
      try {
        const { data } = await axios.get("/api/users/");
        if (!data.success) return toast.error(data.message);

        setConversations(data.users);
      } catch (error) {
        toast.error(error.response.data.message);
      } finally {
        setLoading(false);
      }
    }
    getConversations();
  }, []);

  return { loading, conversations };
};

export default useGetConversations;