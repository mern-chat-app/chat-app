import React from "react";
import { useAuthContext } from "../context/AuthContext";
import axios from "axios";
import toast from "react-hot-toast";

const useLogout = () => {
  const [loading, setLoading] = React.useState(false);
  const { deleteUserDetails } = useAuthContext();

  const logout = async () => {
    setLoading(true);

    try {
      const { data } = await axios.post("/api/auth/logout");

      if (!data.success) return toast.error(data.message);

      deleteUserDetails();
      toast.success(data.message);
    } catch (error) {
      console.log(error);
      toast.error(error.response.data.message)
    } finally {
      setLoading(false);
    }
  };

  return { loading, logout };
};

export default useLogout;