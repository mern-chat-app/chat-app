import React from "react";
import { useSocketContext } from "../context/SocketContext";
import useConversation from "../zustand/useConversation";

import notifySound from "../assets/sounds/notification.mp3";

const useListenMessages = () => {
  const { socket } = useSocketContext();
  const { messages, setMessages } = useConversation();

  React.useEffect(() => {
    socket?.on("newMessage", (newMessage) => {
      newMessage.shouldShake = true;
      const sound = new Audio(notifySound);
      sound.play();
      setMessages([...messages, newMessage]);
    });

    return () => socket.off("newMessage");
  }, [socket, setMessages, messages]);
}

export default useListenMessages