import React from "react";
import useConversation from "../../zustand/useConversation";
import MessageInput from "./MessageInput";
import Messages from "./Messages";
import { TiMessages } from "react-icons/ti";
import { IoCloseCircleSharp } from "react-icons/io5";
import { useAuthContext } from "../../context/AuthContext";

const NoChatSelected = () => {
  const { authUser } = useAuthContext();

  return (
    <div className='flex items-center justify-center w-full h-full'>
      <div className='px-4 text-center sm:text-lg md:text-xl text-gray-200 font-semibold flex flex-col items-center gap-2'>
        <p>Welcome 👋 {authUser.fullname} ❄</p>
        <p>Select a chat to start messaging</p>
        <TiMessages className='text-3xl md:text-6xl text-center' />
      </div>
    </div>
  );
};

const MessageContainer = () => {
  const { selectedConversation, setSelectedConversation } = useConversation();

  React.useEffect(() => {
    return () => setSelectedConversation(null);
  }, [setSelectedConversation]);

  return (
    <div className='md:min-w-[450px] flex flex-col'>
      {
        !selectedConversation ? (
          <NoChatSelected />
        ) : (
          <>
            {/* Header */}
            <div className='bg-slate-500 px-4 py-2 mb-2 flex items-center justify-between'>
              <div className="flex flex-col">
                <span className='text-gray-900 font-bold text-lg'>{selectedConversation.fullname}</span>
                <span className='text-gray-700 font-bold text-sm'>{selectedConversation.username}</span>
              </div>
              <button className="btn" onClick={() => setSelectedConversation(null)}>
                <IoCloseCircleSharp size="28" />
              </button>
            </div>

            <Messages />
            <MessageInput />
          </>
        )
      }
    </div>
  );
};
export default MessageContainer;