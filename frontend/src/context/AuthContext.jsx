import { createContext, useContext, useState } from "react";

export const AuthContext = createContext();

export const useAuthContext = () => {
  return useContext(AuthContext);
};

export const AuthContextProvider = ({ children }) => {
  const [authUser, setAuthUser] = useState(JSON.parse(localStorage.getItem("chat-user")) || null);

  const setUserDetails = data => {
    setAuthUser(data);
    window.localStorage.setItem("chat-user", JSON.stringify(data));
  };

  const deleteUserDetails = () => {
    setAuthUser(null);
    window.localStorage.removeItem("chat-user");
  };

  return <AuthContext.Provider value={{ authUser, setAuthUser, setUserDetails, deleteUserDetails }}>{children}</AuthContext.Provider>
}